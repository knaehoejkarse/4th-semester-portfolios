---
title: "Computational Modeling - Week 3 - Assignment 2 - Part 1"
author: "Riccardo Fusaroli"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## In this assignment we learn how to assess rates from a binomial distribution, using the case of assessing your teachers' knowledge of CogSci

### First part

You want to assess your teachers' knowledge of cognitive science. "These guys are a bunch of drama(turgist) queens, mindless philosophers, chattering communication people and Russian spies. Do they really know CogSci?", you think.

To keep things simple (your teachers should not be faced with too complicated things):
- You created a pool of equally challenging questions on CogSci
- Each question can be answered correctly or not (we don't allow partially correct answers, to make our life simpler).
- Knowledge of CogSci can be measured on a scale from 0 (negative knowledge, all answers wrong) through 0.5 (random chance) to 1 (awesome CogSci superpowers)

This is the data:
- Riccardo: 3 correct answers out of 6 questions
- Kristian: 2 correct answers out of 2 questions (then he gets bored)
- Josh: 160 correct answers out of 198 questions (Josh never gets bored)
- Mikkel: 66 correct answers out of 132 questions

Questions:

1. What's Riccardo's estimated knowledge of CogSci? What is the probability he knows more than chance (0.5) [try figuring this out. if you can't peek into chapters 3.1 and 3.2 and/or the slides]?
- First implement a grid approximation (hint check paragraph 2.4.1!) with a uniform prior, calculate the posterior and plot the results
- Then implement a quadratic approximation (hint check paragraph 2.4.2!).
- N.B. for the rest of the exercise just keep using the grid approximation (we'll move to quadratic approximations in two classes)

```{r}
# define grid (scale from 0 to 1 probabilities between those two, (wrong or correct answer), 10000 the grid between 0 to 1)
p_grid <- seq( from=0 , to=1 , length.out=1000 )
# define prior (repeating 1 a thousand times, 1 = lige så sandsynligt at man svarer rigtigt som forkert, flat distribution)
prior <- rep( 1 , 1000 )
# compute likelihood at each value in grid (3 right answers, 6 questions, prob=p-grid betyder vi vil kun bruge værdier vi har på vores p_grid skalaen, dbinom fordi det er yes/no - wrong/correct questions)
likelihood <- dbinom( 3 , size=6 , prob=p_grid )
# compute product of likelihood and prior
unstd.posterior <- likelihood * prior
# standardize the posterior, so it sums to 1 (så giver sandsynlighederne tilsammen 100%)
posterior <- unstd.posterior / sum(unstd.posterior)

plot( p_grid , posterior , type="b" ,
xlab="probability of Riccardo being right" , ylab="posterior probability" )
mtext( "10000 points" )


##Quadratic approximation:
globe.qa <- map( #mode of a posterior distribution, hvad der optræder flest gange
alist(
r ~ dbinom(6,p) , # binomial likelihood, 6 questions
p ~ dunif(0,1) # uniform prior, 0 to 1 scale
) ,
data=list(r=3) ) #r står for riccardo - vi har 3 rigtige svar
# display summary of quadratic approximation
precis( globe.qa )

#probability he answers above chance? (som at tage arealet af alt over 50% i plottet)
sum( posterior[ p_grid > 0.5 ] )
#there is 50% chance he answers above chance

```


2. Estimate all the teachers' knowledge of CogSci. Who's best? Use grid approximation. Comment on the posteriors of Riccardo and Mikkel.
2a. Produce plots of the prior, and posterior for each teacher.
```{r}
#Riccardo once again - the same as before
# define grid
p_grid <- seq( from=0 , to=1 , length.out=1000 )
# define prior
prior <- rep( 1 , 1000 )
# compute likelihood at each value in grid
likelihood <- dbinom( 3 , size=6 , prob=p_grid )
# compute product of likelihood and prior
unstd.posterior <- likelihood * prior
# standardize the posterior, so it sums to 1
posterior <- unstd.posterior / sum(unstd.posterior)

plot( p_grid , posterior , type="b" ,
xlab="probability of Riccardo being right" , ylab="posterior probability" )
mtext( "6 points" )

plot( p_grid , prior , type="b" ,
xlab="probability of Riccardo being right" , ylab="prior probability" )
mtext( "6 points" )


#Kristian
# define grid
p_grid <- seq( from=0 , to=1 , length.out=10000 )
# define prior
prior <- rep( 1 , 10000 )
# compute likelihood at each value in grid
likelihood <- dbinom( 2 , size=2 , prob=p_grid )
# compute product of likelihood and prior
unstd.posterior <- likelihood * prior
# standardize the posterior, so it sums to 1
posterior <- unstd.posterior / sum(unstd.posterior)

plot( p_grid , posterior , type="b" ,
xlab="probability of Kristian being right" , ylab="prior probability" )
mtext( "2 points" )

plot( p_grid , prior , type="b" ,
xlab="probability of Kristian being right" , ylab="prior probability" )
mtext( "2 points" )

#The Wallen
# define grid
p_grid <- seq( from=0 , to=1 , length.out=10000 )
# define prior
prior <- rep( 1 , 10000 )
# compute likelihood at each value in grid
likelihood <- dbinom( 66 , size=132 , prob=p_grid )
# compute product of likelihood and prior
unstd.posterior <- likelihood * prior
# standardize the posterior, so it sums to 1
posterior <- unstd.posterior / sum(unstd.posterior)

plot( p_grid , posterior , type="b" ,
xlab="probability of Wallentin being right" , ylab="posterior probability" )
mtext( "132 points" )

plot( p_grid , prior , type="b" ,
xlab="probability of Wallentin being right" , ylab="prior probability" )
mtext( "132 points" )

#Here we can see what will happen to the distribution, if we were to ask more questions. Now the distribution shows the normal signs we have come to expect of a gaussian distribution - a very clear global optimum with a symmetrical decline in both sides. It is also noteworthy, that the tails are becoming heavier here than in Riccardo's. In other words, we are more sure about Wallentin than we are about Riccardo. 

#Josh
# define grid
p_grid <- seq( from=0 , to=1 , length.out=10000 )
# define prior
prior <- rep( 1 , 10000 )
# compute likelihood at each value in grid
likelihood <- dbinom( 160 , size=198 , prob=p_grid )
# compute product of likelihood and prior
unstd.posterior <- likelihood * prior
# standardize the posterior, so it sums to 1
posterior <- unstd.posterior / sum(unstd.posterior)

plot( p_grid , posterior , type="b" ,
xlab="probability of Josh being right" , ylab="posterior probability" )
mtext( "198 points" )

plot( p_grid , prior , type="b" ,
xlab="probability of Josh being right" , ylab="prior probability" )
mtext( "198 points" )


```


3. Change the prior. Given your teachers have all CogSci jobs, you should start with a higher appreciation of their knowledge: the prior is a normal distribution with a mean of 0.8 and a standard deviation of 0.2. Do the results change (and if so how)?
3a. Produce plots of the prior and posterior for each teacher.¨
```{r}
#Riccardo
# define grid
p_grid <- seq(from=0, to=1, length.out=10000 )
# define prior (normal distribution, 0.8 mean, 0.2 SD)
prior = dnorm(p_grid, 0.8, 0.2)

#dens(prior)
# compute likelihood at each value in grid
likelihood <- dbinom( 3 , size=6 , prob=p_grid )
# compute product of likelihood and prior
unstd.posterior <- likelihood * prior
# standardize the posterior, so it sums to 1
posterior <- unstd.posterior / sum(unstd.posterior)

plot( p_grid , posterior , type="b" ,
xlab="probability of Riccardo being right" , ylab="posterior probability" )
mtext( "6 points" )

plot( p_grid , prior , type="b" ,
xlab="probability of Riccardo being right" , ylab="prior probability" )
mtext( "6 points" )



#Kristian
# define grid
p_grid <- seq( from=0 , to=1 , length.out=10000 )
# define prior
#sample_sigma = runif(1e4, 0, )
prior = dnorm(p_grid, 0.8, 0.2)
#dens(prior)
# compute likelihood at each value in grid
likelihood <- dbinom( 2 , size=2 , prob=p_grid )
# compute product of likelihood and prior
unstd.posterior <- likelihood * prior
# standardize the posterior, so it sums to 1
posterior <- unstd.posterior / sum(unstd.posterior)

plot( p_grid , posterior , type="b" ,
xlab="probability of Kristian being right" , ylab="prior probability" )
mtext( "2 points" )

plot( p_grid , prior , type="b" ,
xlab="probability of Kristian being right" , ylab="prior probability" )
mtext( "2 points" )

#The Wallen
# define grid
p_grid <- seq( from=0 , to=1 , length.out=10000 )
# define prior
#sample_sigma = runif(1e4, 0, )
prior = dnorm(p_grid, 0.8, 0.2)
#dens(prior)
# compute likelihood at each value in grid
likelihood <- dbinom( 66 , size=132 , prob=p_grid )
# compute product of likelihood and prior
unstd.posterior <- likelihood * prior
# standardize the posterior, so it sums to 1
posterior <- unstd.posterior / sum(unstd.posterior)

plot( p_grid , posterior , type="b" ,
xlab="probability of Wallentin being right" , ylab="posterior probability" )
mtext( "132 points" )

plot( p_grid , prior , type="b" ,
xlab="probability of Wallentin being right" , ylab="prior probability" )
mtext( "132 points" )

#Here we can see what will happen to the distribution, if we were to ask more questions. Now the distribution shows the normal signs we have come to expect of a gaussian distribution - a very clear global optimum with a symmetrical decline in both sides. It is also noteworthy, that the tails are becoming heavier here than in Riccardo's. In other words, we are more sure about Wallentin than we are about Riccardo. 

#Josh
# define grid
p_grid <- seq( from=0 , to=1 , length.out=10000 )
# define prior
#sample_sigma = runif(1e4, 0, )
prior = dnorm(p_grid, 0.8, 0.2)
#dens(prior)
# compute likelihood at each value in grid
likelihood <- dbinom( 160 , size=198 , prob=p_grid )
# compute product of likelihood and prior
unstd.posterior <- likelihood * prior
# standardize the posterior, so it sums to 1
posterior <- unstd.posterior / sum(unstd.posterior)

plot( p_grid , posterior , type="b" ,
xlab="probability of Josh being right" , ylab="posterior probability" )
mtext( "198 points" )

plot( p_grid , prior , type="b" ,
xlab="probability of Josh being right" , ylab="prior probability" )
mtext( "198 points" )


```


4. You go back to your teachers and collect more data (multiply the previous numbers by 100). Calculate their knowledge with both a uniform prior and a normal prior with a mean of 0.8 and a standard deviation of 0.2. Do you still see a difference between the results? Why?
```{r}
#Riccardo
# define grid
p_grid <- seq( from=0 , to=1 , length.out=10000*100 ) #multiplying by 100
# define prior
prior <- rep( 1 , 10000*100 ) #multiplying by 100
# compute likelihood at each value in grid
likelihood <- dbinom( 3*100 , size=6*100 , prob=p_grid ) #multiplying by 100
# compute product of likelihood and prior
unstd.posterior <- likelihood * prior
# standardize the posterior, so it sums to 1
posterior <- unstd.posterior / sum(unstd.posterior)

plot( p_grid , posterior , type="b" ,
xlab="probability of Riccardo being right" , ylab="posterior probability" )
mtext( "6 points" )

plot( p_grid , prior , type="b" ,
xlab="probability of Riccardo being right" , ylab="prior probability" )
mtext( "6 points" )

#Kristian
# define grid
p_grid <- seq( from=0 , to=1 , length.out=10000*100 )
# define prior
prior <- rep( 1 , 10000*100 )
# compute likelihood at each value in grid
likelihood <- dbinom( 2*100 , size=2*100 , prob=p_grid )
# compute product of likelihood and prior
unstd.posterior <- likelihood * prior
# standardize the posterior, so it sums to 1
posterior <- unstd.posterior / sum(unstd.posterior)

plot( p_grid , posterior , type="b" ,
xlab="probability of Kristian being right" , ylab="prior probability" )
mtext( "2 points" )

plot( p_grid , prior , type="b" ,
xlab="probability of Kristian being right" , ylab="prior probability" )
mtext( "2 points" )

#The Wallen
# define grid
p_grid <- seq( from=0 , to=1 , length.out=10000*100 )
# define prior
prior <- rep( 1 , 10000*100 )
# compute likelihood at each value in grid
likelihood <- dbinom( 66*100 , size=132*100 , prob=p_grid )
# compute product of likelihood and prior
unstd.posterior <- likelihood * prior
# standardize the posterior, so it sums to 1
posterior <- unstd.posterior / sum(unstd.posterior)

plot( p_grid , posterior , type="b" ,
xlab="probability of Wallentin being right" , ylab="posterior probability" )
mtext( "132 points" )

plot( p_grid , prior , type="b" ,
xlab="probability of Wallentin being right" , ylab="prior probability" )
mtext( "132 points" )


#Josh
# define grid
p_grid <- seq( from=0 , to=1 , length.out=10000*100 )
# define prior
prior <- rep( 1 , 10000*100 )
# compute likelihood at each value in grid
likelihood <- dbinom( 160*100 , size=198*100 , prob=p_grid )
# compute product of likelihood and prior
unstd.posterior <- likelihood * prior
# standardize the posterior, so it sums to 1
posterior <- unstd.posterior / sum(unstd.posterior)

plot( p_grid , posterior , type="b" ,
xlab="probability of Josh being right" , ylab="posterior probability" )
mtext( "198 points" )

plot( p_grid , prior , type="b" ,
xlab="probability of Josh being right" , ylab="prior probability" )
mtext( "198 points" )

#Riccardo's distribution is still "in the making", as there isn't enough datapoints to show the classical gaussian distribution. Instead of getting a global peak at around 0.5, we get around a 0.36 probability for both 0.4 and 0.6. 

#Riccardo
# define grid
p_grid <- seq(from=0, to=1, length.out=10000*100 )
# define prior
prior = dnorm(p_grid, 0.8, 0.2)

#dens(prior)
# compute likelihood at each value in grid
likelihood <- dbinom( 3*100 , size=6*100 , prob=p_grid )
# compute product of likelihood and prior
unstd.posterior <- likelihood * prior
# standardize the posterior, so it sums to 1
posterior <- unstd.posterior / sum(unstd.posterior)

plot( p_grid , posterior , type="b" ,
xlab="probability of Riccardo being right" , ylab="posterior probability" )
mtext( "6 points" )

plot( p_grid , prior , type="b" ,
xlab="probability of Riccardo being right" , ylab="prior probability" )
mtext( "6 points" )

#Riccardo's distribution is still "in the making", as there isn't enough datapoints to show the classical gaussian distribution. Instead of getting a global peak at around 0.5, we get around a 0.36 probability for both 0.4 and 0.6. 

#Kristian
# define grid
p_grid <- seq( from=0 , to=1 , length.out=10000*100 )
# define prior
#sample_sigma = runif(1e4, 0, )
prior = dnorm(p_grid, 0.8, 0.2)
#dens(prior)
# compute likelihood at each value in grid
likelihood <- dbinom( 2*100 , size=2*100 , prob=p_grid )
# compute product of likelihood and prior
unstd.posterior <- likelihood * prior
# standardize the posterior, so it sums to 1
posterior <- unstd.posterior / sum(unstd.posterior)

plot( p_grid , posterior , type="b" ,
xlab="probability of Kristian being right" , ylab="prior probability" )
mtext( "2 points" )

plot( p_grid , prior , type="b" ,
xlab="probability of Kristian being right" , ylab="prior probability" )
mtext( "2 points" )

#The Wallen
# define grid
p_grid <- seq( from=0 , to=1 , length.out=10000*100 )
# define prior
#sample_sigma = runif(1e4, 0, )
prior = dnorm(p_grid, 0.8, 0.2)
#dens(prior)
# compute likelihood at each value in grid
likelihood <- dbinom( 66*100 , size=132*100 , prob=p_grid )
# compute product of likelihood and prior
unstd.posterior <- likelihood * prior
# standardize the posterior, so it sums to 1
posterior <- unstd.posterior / sum(unstd.posterior)

plot( p_grid , posterior , type="b" ,
xlab="probability of Wallentin being right" , ylab="posterior probability" )
mtext( "132 points" )

plot( p_grid , prior , type="b" ,
xlab="probability of Wallentin being right" , ylab="prior probability" )
mtext( "132 points" )

#Here we can see what will happen to the distribution, if we were to ask more questions. Now the distribution shows the normal signs we have come to expect of a gaussian distribution - a very clear global optimum with a symmetrical decline in both sides. It is also noteworthy, that the tails are becoming heavier here than in Riccardo's. In other words, we are more sure about Wallentin than we are about Riccardo. 

#Josh
# define grid
p_grid <- seq( from=0 , to=1 , length.out=10000*100 )
# define prior
#sample_sigma = runif(1e4, 0, )
prior = dnorm(p_grid, 0.8, 0.2)
#dens(prior)
# compute likelihood at each value in grid
likelihood <- dbinom( 160*100 , size=198*100 , prob=p_grid )
# compute product of likelihood and prior
unstd.posterior <- likelihood * prior
# standardize the posterior, so it sums to 1
posterior <- unstd.posterior / sum(unstd.posterior)

plot( p_grid , posterior , type="b" ,
xlab="probability of Josh being right" , ylab="posterior probability" )
mtext( "198 points" )

plot( p_grid , prior , type="b" ,
xlab="probability of Josh being right" , ylab="prior probability" )
mtext( "198 points" )



```


5. Imagine you're a skeptic and think your teachers do not know anything about CogSci, given the content of their classes. How would you operationalize that belief?
```{r}

```


6. Optional question: Can you estimate the difference between Riccardo's estimated knowledge and that of each of the other teachers? Would you deem it credible (that is, would you believe that it is actually different)?

7. Bonus knowledge: all the stuff we have done can be implemented in a lme4-like fashion using the brms package. Here is an example.
```{r}
library(brms)
d <- data.frame(
  Correct=c(3,2,160,66),
  Questions=c(6,2,198,132),
  Teacher=c("RF","KT","JS","MW"))

FlatModel <- brm(Correct|trials(Questions)~1,data=subset(d,Teacher=="RF"),prior=prior("uniform(0,1)", class = "Intercept"),family=binomial)
plot(FlatModel)
PositiveModel <- brm(Correct|trials(Questions)~1,data=subset(d,Teacher=="RF"),prior=prior("normal(0.8,0.2)", class = "Intercept"),family=binomial)
plot(PositiveModel)
SkepticalModel <- brm(Correct|trials(Questions)~1,data=subset(d,Teacher=="RF"),prior=prior("normal(0.5,0.01)", class = "Intercept"),family=binomial)
plot(SkepticalModel)
```

If you dare, try to tweak the data and model to test two hypotheses:
- Is Kristian different from Josh?
- Is Josh different from chance?




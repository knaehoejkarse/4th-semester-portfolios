---
title: "Eye-tracking data analysis"
author: "Emil R?nn"
date: "7 feb 2019"
output: word_document
---

This a look at the data from the eye-tracking experiments


Preparations: 
```{r setup, include=FALSE}

#Working directory:
#setwd("C:/Users/emilr/OneDrive - Aarhus universitet/Uni/4. Semester/Eye-Tracking/Analysis")

#packages:
library(pacman)
p_load(tidyverse, caret, eyetrackingR)

#loading data:
df <- read.csv("samples2018_2019.csv")

#assessing dataframe:
str(df)

###Changing data formats:

#right eye:
df$RightEye_PositionX = as.character(df$RightEye_PositionX)
df$RightEye_PositionX = as.numeric(df$RightEye_PositionX)

df$RightEye_PositionY = as.character(df$RightEye_PositionY)
df$RightEye_PositionY = as.numeric(df$RightEye_PositionY)

df$RightEye_PupilSize = as.character(df$RightEye_PupilSize)
df$RightEye_PupilSize = as.numeric(df$RightEye_PupilSize)

df$RightEye_MeanVelocityX = as.character(df$RightEye_MeanVelocityX)
df$RightEye_MeanVelocityX = as.numeric(df$RightEye_MeanVelocityX)

df$RightEye_MeanVelocityY = as.character(df$RightEye_MeanVelocityY)
df$RightEye_MeanVelocityY = as.numeric(df$RightEye_MeanVelocityY)

df$RightEye_MeanAccellerationX = as.character(df$RightEye_MeanAccellerationX)
df$RightEye_MeanAccellerationX = as.numeric(df$RightEye_MeanAccellerationX)

df$RightEye_MeanAccellerationY = as.character(df$RightEye_MeanAccellerationY)
df$RightEye_MeanAccellerationY = as.numeric(df$RightEye_MeanAccellerationY)


#Left eye:
df$LeftEye_PositionX = as.character(df$LeftEye_PositionX)
df$LeftEye_PositionX = as.numeric(df$LeftEye_PositionX)

df$LeftEye_PositionY = as.character(df$LeftEye_PositionY)
df$LeftEye_PositionY = as.numeric(df$LeftEye_PositionY)

df$LeftEye_PupilSize = as.character(df$LeftEye_PupilSize)
df$LeftEye_PupilSize = as.numeric(df$LeftEye_PupilSize)

df$LeftEye_MeanVelocityX = as.character(df$LeftEye_MeanVelocityX)
df$LeftEye_MeanVelocityX = as.numeric(df$LeftEye_MeanVelocityX)

df$LeftEye_MeanVelocityY = as.character(df$LeftEye_MeanVelocityY)
df$LeftEye_MeanVelocityY = as.numeric(df$LeftEye_MeanVelocityY)

df$LeftEye_MeanAccellerationX = as.character(df$LeftEye_MeanAccellerationX)
df$LeftEye_MeanAccellerationX = as.numeric(df$LeftEye_MeanAccellerationX)

df$LeftEye_MeanAccellerationY = as.character(df$LeftEye_MeanAccellerationY)
df$LeftEye_MeanAccellerationY = as.numeric(df$LeftEye_MeanAccellerationY)

```

Running the analysis:
```{r}

```


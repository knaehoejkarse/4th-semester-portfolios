---
title: "Assignment 4 Emil"
author: "Emil Rønn"
date: "19 mar 2019"
output: word_document
---

Preperations
```{r}
library(pacman)
p_load(BiocManager, pastecs, tidyverse, brms, rethinking, rstan, caret, dagitty, rgl, corrplot, RColorBrewer, Hmisc)
setwd("~/Studiegruppe/4th-semester/Assignment 4 Causal inference")
```

Data prep
```{r}
data <- read_csv("CausalinferenceData.csv")
str(data)
```

Analysis:
```{r}

```

